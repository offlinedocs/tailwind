# Build
FROM node:lts AS builder

RUN git clone https://github.com/tailwindlabs/tailwindcss.com.git /docs

WORKDIR /docs

RUN yarn
RUN yarn export

FROM nginx:alpine

COPY --from=builder /docs/out /usr/share/nginx/html
